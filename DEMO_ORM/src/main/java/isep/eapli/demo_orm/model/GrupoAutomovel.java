package isep.eapli.demo_orm.model;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class GrupoAutomovel {

    public int numPortas = 0;

    @Id
    private String name;
    private String classe;

    public GrupoAutomovel (){
    }

    public GrupoAutomovel ( String nome, int portas, String classe ){
        this.name = nome;
        this.numPortas = portas;
        this.classe = classe;
    }


    public void setNumPortas ( int numPortas ){
        this.numPortas = numPortas;
    }

    public String getClasse (){
        return classe;
    }

    public void setClasse ( String classe ){
        this.classe = classe;
    }

    @Override
    public String toString (){
        return "GrupoAutomovel{" +
                "numPortas=" + numPortas +
                ", classe='" + classe + '\'' +
                '}';
    }
}
