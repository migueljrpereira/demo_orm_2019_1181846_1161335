package isep.eapli.demo_orm.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Automovel {

    @Id
    private String matricula = "fa-00-ke";

    private int km;


    public int getKm (){
        return km;
    }

    public void setKm ( int km ){
        this.km = km;
    }

    public String getMatricula (){
        return matricula;
    }

    @java.lang.Override
    public java.lang.String toString (){
        return "Automovel{" +
                "km=" + km +
                ", matricula='" + matricula + '\'' +
                "'} <- NOT REAL";
    }
}
